import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ListingsComponent } from './_components/listings/listings.component';
import { LoginComponent } from './_components/login/login.component';

import { AuthInterceptor } from './_interceptors/auth-interceptor';
import { HeaderComponent } from './_components/header/header.component';
import { HomeComponent } from './_components/home/home.component';
import { MyProfileComponent } from './_components/my-profile/my-profile.component';
import { ProfileComponent } from './_components/profile/profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './_components/register/register.component';
import { ProfilesComponent } from './_components/profiles/profiles.component';
import { ListingCreateComponent } from './_components/listing-create/listing-create.component';
import { ListingEditComponent } from './_components/listing-edit/listing-edit.component';
import { ListingDetailComponent } from './_components/listing-detail/listing-detail.component';
import { ListingService } from './_services/listing.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ImageReaderComponent } from './_components/image-reader/image-reader.component';
import { ProfileDetailComponent } from './_components/profile-detail/profile-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    ListingsComponent,
    LoginComponent,
    HeaderComponent,
    HomeComponent,
    MyProfileComponent,
    ProfileComponent,
    RegisterComponent,
    ProfilesComponent,
    ListingCreateComponent,
    ListingEditComponent,
    ListingDetailComponent,
    ImageReaderComponent,
    ImageReaderComponent,
    ProfileDetailComponent,
  ],
  imports: [
    FontAwesomeModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    ListingService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
