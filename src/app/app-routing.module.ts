import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './_components/home/home.component';
import { ListingCreateComponent } from './_components/listing-create/listing-create.component';
import { ListingDetailComponent } from './_components/listing-detail/listing-detail.component';
import { ListingEditComponent } from './_components/listing-edit/listing-edit.component';
import { ListingsComponent } from './_components/listings/listings.component';
import { LoginComponent } from './_components/login/login.component';
import { MyProfileComponent } from './_components/my-profile/my-profile.component';
import { ProfileComponent } from './_components/profile/profile.component';
import { ProfilesComponent } from './_components/profiles/profiles.component';
import { RegisterComponent } from './_components/register/register.component';
import { ProfileDetailComponent } from './_components/profile-detail/profile-detail.component';

const routes: Routes = [
  // {path: 'home', component: HomeComponent},

  {path: 'listings', component: ListingsComponent},
  {path: 'listings/:id', component: ListingDetailComponent},
  // {path: 'listings/:id/edit', component: ListingEditComponent},
  // {path: 'listings/create', component: ListingCreateComponent},
  {path: 'create', component: ListingCreateComponent},
  
  {path: 'profiles', component: ProfilesComponent},
  {path: 'profiles/:id', component: ProfileDetailComponent},

  {path: 'my-profile', component: MyProfileComponent},

  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: '**', redirectTo:'listings', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
