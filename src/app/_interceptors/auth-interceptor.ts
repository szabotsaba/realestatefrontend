import { HttpHandler, HttpRequest, HttpEvent, HttpInterceptor, HttpErrorResponse } from "@angular/common/http";
import { Observable, BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";
import { AuthService } from "../_services/auth.service";
import { throwError } from "rxjs";
import { catchError, filter, take, switchMap } from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private authService: AuthService){}

  intercept(req: HttpRequest<any>,
    next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.authService.getAccessToken();
  
    if (token) {
      req = this.addToken(req, token);
    }
    return next.handle(req)
      .pipe(
        catchError(error => {
          if (error instanceof HttpErrorResponse && error.status === 401) {
            return this.handle401Error(req, next);
          } else {
            return throwError(error);
          }
      }));
  }


   private addToken(req: HttpRequest<any>, token: string){
       return req.clone({
           headers: req.headers.set("Authorization",
               "Bearer " + token)
       });
   }

   private handle401Error(req: HttpRequest<any>, next: HttpHandler){
     if (!this.isRefreshing) {
       this.isRefreshing = true;
       this.refreshTokenSubject.next(null);
 
       return this.authService.refresh()
        .pipe(
          switchMap((token: any) => {
            this.isRefreshing = false;
            this.refreshTokenSubject.next(token.access);
            return next.handle(this.addToken(req, token.access));
          }));
  
      } else {
        return this.refreshTokenSubject
          .pipe(
            filter(token => token != null),
            take(1),
            switchMap(jwt => {
              return next.handle(this.addToken(req, jwt));
      }));
    }
  }
}
