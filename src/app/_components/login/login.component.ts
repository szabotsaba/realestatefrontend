import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_interfaces/user';
import { UserService } from 'src/app/_services/user.service';
import { AuthService } from 'src/app/_services/auth.service';
import { SessionService } from 'src/app/_services/session.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email : new FormControl('',Validators.required),
    password: new FormControl('', Validators.required),
  })
  failedToLogIn :boolean = false;

  constructor(private authService : AuthService,
    private sessionSevice: SessionService,
    private router: Router,) { 
      this.sessionSevice._failedToLogIn.subscribe(data => this.failedToLogIn=data);
      this.sessionSevice._isLoggedIn.subscribe(data=>{
        if(data){
          this.router.navigate(['/home']);    
        }
      }) 
  }

  ngOnInit(): void {
    this.sessionSevice._failedToLogIn.next(false);
  }

  login(){
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password).subscribe();
    this.clear();
  }

  logout(){
    this.authService.logout()
  }

  private clear(){
      this.loginForm.reset();
  }
}
