import { Component, OnInit } from '@angular/core';
import { ListingService, rez } from 'src/app/_services/listing.service';
import { Listing } from 'src/app/_interfaces/listing';
import { ImageService } from 'src/app/_services/image.service';

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.scss']
})
export class ListingsComponent implements OnInit {

  listings : Listing[] = [];

  // listing : Listing = {
  //   id: 0,
  //   title: '',
  //   description: ''
  // }

  constructor(private listingService : ListingService,
    private imageService: ImageService) {}
    

  ngOnInit(): void {
    this.showListings();
  }

  showListings() {
    this.listingService.getAllListings()
      .subscribe(data => this.listings = data);
      }

  // deleteListing(id: number) {
  //   this.listingService.deleteListing(id).subscribe();
  // }

  // updateListing(listing: Listing) {
  //   let list : Listing = {
  //     id: listing.id,
  //     title: this.listing.title,
  //     description: this.listing.description,
  //     owner: '',
  //   }
  //   this.listingService.updateListing(list).subscribe();
  // }

  // deleteImage(url: string){
  //   this.imageService.deleteImageByUrl(url).subscribe();
  // }
}
