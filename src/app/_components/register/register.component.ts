import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { lastValueFrom } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionService } from 'src/app/_services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  wrong = false;
  registerForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
      // Validators.pattern("^[a-z0-9._%+-]")]),
    email: new FormControl('', [Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    password: new FormControl('', [Validators.required]),
    // Validators.pattern("^[a-z0-9._%+-]")])

  })

  constructor(private authService: AuthService,
    private sessionService: SessionService,
    private router: Router) { 
    this.sessionService._isLoggedIn.subscribe(data=>{
      if(data){
        this.router.navigate(['/home']);    
      }
    })
    this.sessionService._usernameOrEmailWrong.subscribe(data => this.wrong=data) 
  }

  ngOnInit(): void {
    this.sessionService._usernameOrEmailWrong.next(false);
  }
  
  register(){
    lastValueFrom(this.authService.register({username: this.registerForm.value.username, email: this.registerForm.value.email, password: this.registerForm.value.password})).then(
    () =>  {
      if(!this.wrong){
        this.authService.login(this.registerForm.value.email, this.registerForm.value.password).subscribe()
        this.registerForm.reset()
      }})
  }

}
