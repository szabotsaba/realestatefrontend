import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { SessionService } from 'src/app/_services/session.service';
import { faAnglesRight, faAnglesDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  faAnglesRight = faAnglesRight;
  faAnglesDown = faAnglesDown;
  isMenu=false;
  
  isAuthenticated: boolean = false;
  
  constructor(private authService: AuthService,
    private sessionService: SessionService) {
    this.sessionService._isLoggedIn.subscribe( value => {
      this.isAuthenticated = value;
  });
   }

  ngOnInit(): void {
  }

  showMenu(){
    this.isMenu=!this.isMenu;
  }

  logout(){
    this.authService.logout();
  }
}
