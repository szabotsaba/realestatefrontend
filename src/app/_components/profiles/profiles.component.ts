import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/_interfaces/profile';
import { ProfileService } from 'src/app/_services/profile.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {

  profiles: Profile[] = []
  currentUser: string = '';

  constructor(private profileService: ProfileService,
    private userService : UserService) { }

  ngOnInit(): void {
    this.getProfiles();
  }

  getProfiles(){
    this.profileService.getAllProfiles().subscribe(data => this.profiles = data)
    this.userService.getCurrentUser().subscribe(data => {
      if(data.username){
        this.currentUser = data.username
      }
    })
  }

}
