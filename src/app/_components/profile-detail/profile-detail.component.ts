import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Profile } from 'src/app/_interfaces/profile';
import { ProfileService } from 'src/app/_services/profile.service';

@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: ['./profile-detail.component.scss']
})
export class ProfileDetailComponent implements OnInit {

  profile?: Profile
  constructor(private route: ActivatedRoute,
    private profileService: ProfileService) { }

  ngOnInit(): void {
   this.getListing();
  }

  getListing() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.profileService.getProfileById(id)
      .subscribe(data => this.profile=data);
  }


}
