import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Profile } from 'src/app/_interfaces/profile';
import { User } from 'src/app/_interfaces/user';
import { ProfileService } from 'src/app/_services/profile.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profile?: Profile;

  constructor( private route: ActivatedRoute,
    private profileService: ProfileService) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(){
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.profileService.getProfileById(id)
      .subscribe(data => this.profile=data);
  }
}
