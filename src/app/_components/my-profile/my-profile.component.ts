import { Component, OnInit } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { ImageToUpload } from 'src/app/_interfaces/image';
import { Listing } from 'src/app/_interfaces/listing';
import { Profile } from 'src/app/_interfaces/profile';
import { User } from 'src/app/_interfaces/user';
import { ListingService } from 'src/app/_services/listing.service';
import { ProfileService } from 'src/app/_services/profile.service';
import { UserService } from 'src/app/_services/user.service';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { SessionService } from 'src/app/_services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {

  faTrash = faTrashAlt;
  user?: User;
  image?: ImageToUpload;
  profile?: Profile;

  constructor(private userService: UserService,
    private listingService: ListingService,
    private profileService: ProfileService,
    private sessionService: SessionService,
    private router: Router) {
    this.sessionService._isLoggedIn.subscribe(data=>{
      if(!data){
        this.router.navigate(['/home']);    
      }
    })
     }

  ngOnInit(): void {
    this.getUser().then(()=>this.profile = this.user?.profile)
  }

  async getUser(){    
    this.user = await lastValueFrom(this.userService.getCurrentUser());
  }

  deleteListing(listing: Listing){
    lastValueFrom(this.listingService.deleteListing(listing)).then(
      () => this.ngOnInit()
    )
  }

  addProfilePicture(img: ImageToUpload){
    this.image=img;
  }

  // updateProfile(){
  //   if(this.profile && this.image){
  //     this.profile.image = this.image.image;
  //     console.log(this.image);
      
  //     this.profileService.updateProfile(this.profile).subscribe();
  //     console.log('bar');
  //     return
  //   }
  //   console.log('foo');
    
  // }
}
