import { Component, OnInit } from '@angular/core';
import { Listing } from 'src/app/_interfaces/listing';
import { ListingService } from 'src/app/_services/listing.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ListingImage, ImageToUpload } from 'src/app/_interfaces/image';
import { ImageService } from 'src/app/_services/image.service';
import { lastValueFrom } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-listing-edit',
  templateUrl: './listing-edit.component.html',
  styleUrls: ['./listing-edit.component.scss']
})
export class ListingEditComponent implements OnInit {

  faTrash = faTrashAlt;
  listingForm = new FormGroup({
    title: new FormControl('',Validators.required),
    price: new FormControl('',Validators.required),
    description: new FormControl('',Validators.required),
    rooms: new FormControl('',Validators.required),
    surface: new FormControl('',Validators.required),
    thumbnail: new FormControl('',Validators.required)
  })
  listing?: Listing;
  images: ImageToUpload[] = [];

  constructor(private listingService: ListingService,
    private route: ActivatedRoute,
    private imageService: ImageService,
    private router: Router ) { }

  ngOnInit(): void {
    this.getListing().then(()=>this.setForm());
  }

  async getListing() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.listing = await lastValueFrom(this.listingService.getListingById(id));
  }

  deleteListing() {
    if (this.listing){
      lastValueFrom(this.listingService.deleteListing(this.listing)).then(
        () => this.router.navigate(['//my-profile'])
      )
    }
  }

  updateListing() {
    if (this.listing){
      this.listingService.updateListing(this.listing).subscribe();
    }
  }

  deleteListingImage(listingImage: ListingImage){
    lastValueFrom(this.imageService.deleteListingImage(listingImage)).then(
      () => this.ngOnInit())
  }

  deleteImage(image: string){
    this.images.forEach( (item, index) => {
      if(item.image === image && this.images) this.images.splice(index,1);
    });
  }

  pushListingImage(image: ImageToUpload){
    let isImg = true;
    this.images.forEach( (item, index) => {
      if(item.image === image.image && this.images){
        isImg = false;
      };
    });
    if(isImg){
      this.images.push(image);
    }
  }


  submit(){
    this.updateListing();
  }

  setForm(){
    this.listingForm.setValue({title: this.listing?.title,
      price: this.listing?.price,
    description: this.listing?.description,
    rooms: this.listing?.rooms,
    surface: this.listing?.surface,
    thumbnail: this.listing?.thumbnail
    })
  }

}
