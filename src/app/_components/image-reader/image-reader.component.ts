import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ImageToUpload } from 'src/app/_interfaces/image';
import { faPaperclip } from '@fortawesome/free-solid-svg-icons';

const MAX_SIZE: number = 5 * 1048576;

@Component({
  selector: 'app-image-reader',
  templateUrl: './image-reader.component.html',
  styleUrls: ['./image-reader.component.scss']
})
export class ImageReaderComponent implements OnInit {

  @Output() newImageEvent= new EventEmitter<ImageToUpload>();
  image?: ImageToUpload;
  messages: string[] = [];
  theFiles: File[]=[];
  imageBase64: string = '';
  faPaperclip=faPaperclip;

  constructor(){}

  ngOnInit(): void {
      
  }

  onFileChange(event:any) {
    // Any file(s) selected from the input?
    const files = event.target.files;
    for(let i=0; i<files.length; ++i){
      // Don't allow file sizes over 5MB
            if (files[i].size < MAX_SIZE) {
                // Add file to list of files
                this.theFiles.push(files[i]);
                this.readImage();
            }
            else {
                this.messages.push("File: " + files[i].name + " is too large to upload.");
            }
    };
  }

    addNewImage() {
      this.newImageEvent.emit(this.image);
    }



  private readImage() {
    const reader = new FileReader();
    this.theFiles.forEach((fil: File) => {
      const file = this.theFiles.pop();
      if(file){
        reader.readAsDataURL(file);
        var name = file.name
      }
      reader.onload = (e: any) => {
        if(reader.result){
          this.imageBase64 = reader.result?.toString();
          this.image = {
            name: name,
            image: this.imageBase64,
          }
          this.addNewImage();
        }
        };
      
    });


  };

  
}
