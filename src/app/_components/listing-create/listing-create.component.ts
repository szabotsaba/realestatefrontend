import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ListingToUpload } from 'src/app/_interfaces/listing';
import { ImageToUpload } from 'src/app/_interfaces/image';
import { ListingService } from 'src/app/_services/listing.service';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { SessionService } from 'src/app/_services/session.service';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';

@Component({
  selector: 'app-listing-create',
  templateUrl: './listing-create.component.html',
  styleUrls: ['./listing-create.component.scss']
})
export class ListingCreateComponent implements OnInit {

  faTrash = faTrashAlt;
  doesListingExist = false;
  listingForm = new FormGroup({
    title: new FormControl('',Validators.required),
    price: new FormControl('',Validators.required),
    description: new FormControl('',Validators.required),
    rooms: new FormControl('',Validators.required),
    surface: new FormControl('',Validators.required),
    thumbnail: new FormControl('',Validators.required)
  })

  images: ImageToUpload[] = [];

  constructor(private listingService : ListingService,
    private sessionService: SessionService,
    private router: Router) {
    this.sessionService._isLoggedIn.subscribe(data=>{
      if(!data){
        this.router.navigate(['/home']);    
      }
    })
    this.sessionService._doesListingExist.subscribe(data => this.doesListingExist = data); 
  }

  ngOnInit(): void {
  }

  pushListingImage(image: ImageToUpload){
    let isImg = true;
    this.images.forEach( (item, index) => {
      if(item.image === image.image && this.images){
        isImg = false;
      };
    });
    if(isImg){
      this.images.push(image);
    }
  }

  createListing(listing: ListingToUpload) {
      lastValueFrom(this.listingService.createListing(listing)).then(()=>{
        if(!this.doesListingExist){
          this.listingForm.reset();
        }
      });
  }

  deleteListingImage(image: string){
    this.images.forEach( (item, index) => {
      if(item.image === image && this.images) this.images.splice(index,1);
    });
  }

  changeThumbnail(e:any){
    console.log(e.target.value);
  }

  submit(){
    this.deleteListingImage(this.listingForm.value.thumbnail)
    const listing: ListingToUpload = {
      title: this.listingForm.value.title,
      description: this.listingForm.value.description,
      price: this.listingForm.value.price,
      surface: this.listingForm.value.surface,
      thumbnail: this.listingForm.value.thumbnail,
      rooms: this.listingForm.value.rooms,
      images: this.images
    }
    this.createListing(listing);
    this.images = [];
  }
}
