import { Component, OnInit } from '@angular/core';
import { Listing } from 'src/app/_interfaces/listing';
import { ListingService } from 'src/app/_services/listing.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-listing-detail',
  templateUrl: './listing-detail.component.html',
  styleUrls: ['./listing-detail.component.scss']
})
export class ListingDetailComponent implements OnInit {

  listing?: Listing

  constructor(private listingService: ListingService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getListing();
  }

  getListing() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.listingService.getListingById(id)
      .subscribe(data => this.listing=data);
  }

}
