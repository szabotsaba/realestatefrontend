
// export const API_ADRESS = '//192.168.43.167:8000/';
export const API_ADRESS = '//localhost:8000/';
// export const API_ADRESS = '//192.168.68.101:8000/';
export const API_URL = API_ADRESS + 'api/';

export const TOKEN_URL = API_URL + 'token/';
export const TOKEN_REFRESH_URL = API_URL + 'token/refresh/';

export const LISTING_IMAGES_URL = API_URL + 'listing-images/';

export const LISTINGS_URL = API_URL + "listings/";

export const USERS_URL = API_URL + 'users/';

export const PROFILES_URL =  API_URL + 'profiles/';