import { Injectable } from '@angular/core';
import { User, UserToUpload } from '../_interfaces/user';
import { HttpClient } from '@angular/common/http';
import { shareReplay, map, tap, Observable, catchError } from 'rxjs';
import { UserService } from './user.service';
import { SessionService } from './session.service';
import { TOKEN_URL, TOKEN_REFRESH_URL } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _tokenURL = TOKEN_URL;
  private _refreshURL = TOKEN_REFRESH_URL;

  constructor(private http: HttpClient,
    private userService: UserService,
    private sessionService: SessionService) {
      this.sessionService._isLoggedIn.next(this.isLoggedIn())
  }

  register(user: UserToUpload) : Observable<User> {
    return this.userService.createUser(user); 
  }

  login(email: string, password: string) {
      return this.http.post(this._tokenURL, {email: email, password: password})
          .pipe(
            tap((res: any) => {
            this.setTokens(res);
            this.sessionService._isLoggedIn.next(true);
          }),
            shareReplay(),
            catchError(async () => {
              this.sessionService._isLoggedIn.next(false);
              this.sessionService._failedToLogIn.next(true);
            })
            );
  }

  refresh() {
    return this.http.post(this._refreshURL, {refresh: localStorage.getItem('refresh')})
    .pipe(
      tap((res: any) => 
      this.setTokens(res)),
      shareReplay());
  }

  logout() {
      localStorage.removeItem('access');
      localStorage.removeItem('refresh');
      this.sessionService._isLoggedIn.next(false);
  }

  private setTokens(res:any){
    localStorage.setItem('access', res.access);
    localStorage.setItem('refresh', res.refresh);
  }

  getAccessToken(){
    return localStorage.getItem('access');
  }

  getRefreshToken(){
    return localStorage.getItem('refresh');
  }

   public isLoggedIn() {
       return !!localStorage.getItem('access');
 }

   public isLoggedOut() {
       return !this.isLoggedIn();
   }

}
