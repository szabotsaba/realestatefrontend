import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { User, UserToUpload } from '../_interfaces/user';
import { TokenService } from './token.service';
import { map, tap, switchMap, catchError } from 'rxjs/operators'; 
import { Listing } from '../_interfaces/listing';
import { Observable, of } from 'rxjs';
import { ListingService } from './listing.service';
import { ProfileService } from './profile.service';
import { Profile } from '../_interfaces/profile';
import { lastValueFrom } from 'rxjs';
import { SessionService } from './session.service';
import { USERS_URL } from '../constants';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private _usersUrl = USERS_URL;

    constructor(private tokenService: TokenService,
        private listingService: ListingService,
        private profileService: ProfileService,
        private http: HttpClient,
        private sessionService: SessionService){}


    getCurrentUser(): Observable<User>{        
        return this.http.get<User>(this._usersUrl+this.getCurrentUserId()).pipe(
            switchMap(async (data:any) =>{
                const user = await this.handleUser(data)
                return user
            } 
            ))
    }

    createUser(user: UserToUpload): Observable<User>{
        return this.http.post<User>(this._usersUrl, user).pipe(
            tap(()=>this.sessionService._usernameOrEmailWrong.next(false)),
            catchError((err) :Observable<User> => {
                this.sessionService._usernameOrEmailWrong.next(true);
                return of(err as User);
            })
        )
    }

    deleteUser(user: User) {
        let use: User | undefined
        this.getCurrentUser().subscribe(data => use=data)
        if (use==user)
            return this.http.delete(user.url)
        return console.error("cannot delete this user");
        
    }

    private async handleUser(res: any): Promise<User>{
        // let listing_list: Listing[] = [];
        // res.favorites.forEach((listing: any) => {
        //     this.listingService.getListingByUrl(listing.url).subscribe((data : Listing) => listing_list.push(data));      
        //   });
        const profile: Profile = await lastValueFrom(this.profileService.getProfileByUrl(res.profile));
        
        let user: User = {
            url: res.url,
            username: res.username,
            email: res.email,
            // favorites: listing_list,
            profile: profile
        }
        
        return user
    }
    
    getCurrentUserId(): string{        
        return this.tokenService.getDecodedAccessToken().user_id.toString();
    }
}