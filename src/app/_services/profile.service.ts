import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { tap, map, of, catchError, firstValueFrom } from 'rxjs';
import { Profile } from '../_interfaces/profile';
import { Listing } from '../_interfaces/listing';
import { ListingService } from './listing.service';
import { PROFILES_URL } from '../constants';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private _profileUrl = PROFILES_URL;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   **/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  constructor(private http: HttpClient,
    private listingService: ListingService) { }
  
  /** GET Listings from the server */
  getProfilesByPage(page_number: number): Observable<Profile[]> {
    return this.http.get<Profile[]>(this._profileUrl+'/?page='+page_number)
      .pipe(
        map((res:any) => {
          let profiles: Profile[] = [];
            res.results.forEach((profile: any) => {
                profiles.push(this.handleProfile(profile));
            });
            return profiles;
        }),
        catchError(this.handleError<Profile[]>('getProfiles', undefined))
      );
}

  /** GET Listing by id. Will 404 if id not found */
  getProfileById(id: number): Observable<Profile> {
    return this.getProfileByUrl(this._profileUrl+id+'/');
  }

  /** PUT: update the Listing on the server */
  updateProfile(profile: Profile): Observable<any> {
    return this.http.put(profile.url, profile).pipe(
      catchError(this.handleError<any>('updateListing'))
    );
  }

  getProfileByUrl(url: string): Observable<Profile>{
    return this.http.get<Profile>(url).pipe(
      map((res: any) => res = this.handleProfile(res))
      )
  }

  getAllProfiles(i=1): Observable<Profile[]>{
    return this.http.get<Profile[]>(this._profileUrl+'?page='+i).pipe(
        map ((res : any) => {
            let profiles: Profile[] = [];
            res.results.forEach((profile: any) => {
                profiles.push(this.handleProfile(profile))
            });
            if (res.next) {
                this.getAllProfiles(i++).subscribe(data => profiles = profiles.concat(data));
            }
            return profiles;
        })
    )
}

  private handleProfile(res: any): Profile {
    let list: Listing[] = [];
    res.listings.forEach( async (listingUrl: string) => {
      const listing = await firstValueFrom(this.listingService.getListingByUrl(listingUrl));
      list.push(listing);      
    });
    let profile: Profile = {
      id: res.id,
      url: res.url,
      // image: res.image,
      user: res.user,
      listings: list
    }
    return profile;
  }
}