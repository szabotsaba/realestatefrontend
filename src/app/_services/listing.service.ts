import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import { Listing, ListingToUpload } from '../_interfaces/listing';
import { ImageService } from './image.service';
import { ListingImage } from '../_interfaces/image';
import { LISTINGS_URL } from '../constants';
import { SessionService } from './session.service';

export interface rez {
  count: number;
  next: string;
  previous: string;
  results: [];
}

@Injectable({
  providedIn: 'root'
})

export class ListingService {

  private _listingsUrl = LISTINGS_URL;  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  // httpUploadOptions = {
  //   headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
  // };

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   **/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  constructor(private http: HttpClient,
    private imageService: ImageService,
    private sesssionService: SessionService) { }
  
  /** GET Listings from the server */
  getListingsByPage(page_number: number): Observable<Listing[]> {
    return this.http.get<Listing[]>(this._listingsUrl+'/?page='+page_number)
      .pipe(
        map((res:any) => {
          let listings: Listing[] = [];
            res.results.forEach((listing: any) => {
                listings.push(this.handleListing(listing));
            });
            return listings;
        }),
        catchError(this.handleError<Listing[]>('getListings', undefined))
      );
}

  /** GET Listing by id. Will 404 if id not found */
  getListingById(id: number): Observable<Listing> {
    return this.getListingByUrl(this._listingsUrl+id+'/');
  }

  /** PUT: update the Listing on the server */
  updateListing(listing: Listing): Observable<any> {
    return this.http.put(listing.url, listing, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateListing'))
    );
  }

  /** POST: add a new Listing to the server */
  createListing(listing: ListingToUpload) {
    return this.http.post<Listing>(this._listingsUrl, listing, this.httpOptions).pipe(
      tap(()=>this.sesssionService._doesListingExist.next(false)),
      catchError(async err => {
        this.sesssionService._doesListingExist.next(true);
      })
    );
  }

  /** DELETE: delete the Listing from the server */
  deleteListing(listing: Listing): Observable<Listing> {
    return this.http.delete<Listing>(listing.url, this.httpOptions).pipe(
      catchError(this.handleError<Listing>('deleteListing'))
    );
  }

  getListingByUrl(url: string): Observable<Listing>{
    return this.http.get<Listing>(url).pipe(
      map((res: any) => res = this.handleListing(res))
      )
  }

  getAllListings(i=1): Observable<Listing[]>{
    return this.http.get<Listing[]>(this._listingsUrl+'?page='+i).pipe(
        map ((res : any) => {
            let listings: Listing[] = [];
            res.results.forEach((listing: any) => {
                listings.push(this.handleListing(listing))
            });
            if (res.next) {
                this.getAllListings(i++).subscribe(data => listings = listings.concat(data));
            }
            return listings;
        })
    )
}

  private handleListing(res: any): Listing {
    let list: ListingImage[] = [];
    res.images.forEach((image: ListingImage) => {
      // this.imageService.getImageByUrl(image).subscribe((data : ListingImage) => list.push(data));      
      list.push(image);
    });
    let listing: Listing = {
      url: res.url,
      id: res.id,
      title: res.title,
      description: res.description,
      owner: res.owner,
      rooms: res.rooms,
      surface: res.surface,
      price: res.price,
      pub_date: res.pub_date,
      images_ready: list,
      thumbnail: res.thumbnail
    }
    return listing;
  }
}
