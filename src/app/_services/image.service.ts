import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { ListingImage } from '../_interfaces/image';
import { Observable } from 'rxjs';
import { LISTING_IMAGES_URL } from '../constants';

@Injectable({ 
  providedIn: 'root'
})
export class ImageService {

  _lisingImagesURL = LISTING_IMAGES_URL;

  constructor(private http: HttpClient) { }

  getListingImagesDetail(page: string = ''): Observable<ListingImage[]>{
    return this.http.get<ListingImage[]>(this._lisingImagesURL + page).pipe(
      map((res: any) => {
        let list: ListingImage[] =[];
        res.results.forEach((res:any) => {
          list.push(this.handleListingImage(res));
        });
        return list;
      })
    )
  }

  getListingImageById(id: number){
    return this.http.get<ListingImage>(this._lisingImagesURL + id).pipe(
      map((res: any) => {
        return this.handleListingImage(res);
      })
    )
  }

  addListingImage(data: FormData){
    return this.http.post<File>(this._lisingImagesURL, data)
  }

  deleteListingImage(listingImage: ListingImage){
    return this.http.delete(listingImage.url);
  }

  private handleListingImage(res : any): ListingImage{
    let image: ListingImage = {
      url: res.url,
      id: res.id,
      owner: res.owner,
      image: res.image
  }
  return image;
  }

  getImageByUrl(url: string): Observable<ListingImage>{
    return this.http.get<ListingImage>(url).pipe(
      map((res: any)=> this.handleListingImage(res))
    )
  }
  
  deleteImageByUrl(url: string){
    return this.http.delete(url);
  }

}
