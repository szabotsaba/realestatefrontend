import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  public _isLoggedIn = new BehaviorSubject<boolean>(false);
  public _failedToLogIn = new BehaviorSubject<boolean>(false);
  public _usernameOrEmailWrong = new BehaviorSubject<boolean>(false);
  public _doesListingExist = new BehaviorSubject<boolean>(false);

  constructor() { }
}
