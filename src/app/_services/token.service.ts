import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  private getDecodedToken(token:string | null):any{
    try {
      if(token){
        return jwt_decode(token);
      }
      } catch(error){
      return null;
    } 
  }
  
  getDecodedRefreshToken() {
    return this.getDecodedToken(localStorage.getItem('refresh'));
  }

  getDecodedAccessToken() {    
    return this.getDecodedToken(localStorage.getItem('access'));
  }
  
}
