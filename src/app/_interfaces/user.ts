import { Profile } from './profile'
import { Listing } from "./listing";

export interface User {
    url:string;
    id?: number;
    username?: string;
    // password?: string;
    email?: string;
    // favorites?: Listing[];
    profile: Profile;
}

export interface UserToUpload {
    username: string;
    email: string;
    password: string;
}