import { ListingImage, ImageToUpload } from './image'

export interface Listing {
  url: string;
  owner: string;
  id: number;
  title: string;
  description: string;
  images?: ImageToUpload[];
  images_ready?: ListingImage[];
  surface: number;
  thumbnail: string;
  rooms: number;
  price: number;
  pub_date: string;
}

export interface ListingToUpload {
  title?: string;
  description?: string;
  images?: ImageToUpload[];
  surface?: number;
  rooms?: number;
  price?: number;
  thumbnail?: string;
}