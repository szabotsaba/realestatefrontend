import { Listing } from "./listing";

export interface Profile {
    id: number;
    url: string;
    // image: string;
    listings: Listing[];
    user: string;
}