
export interface ListingImage {
    url: string,
    id?: number,
    owner?: number,
    image: string
}


export interface ImageToUpload {
    name: string,
    image: string,
}